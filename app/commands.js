const unit = require('ethjs-unit')
const abi = require('ethjs-abi');
import util from './util.js'

var docs = {}
docs["/online"] = "Display all users who are online."
const getOnline = (send, room) =>{
    return send({cmd: 'getOnline', room: room})
}

const help = (addChatEntry) => { //TODO allow options to display individual e.g. /help --contracts
    return addChatEntry({text: docs, type: "help"});
}

docs["/info"] = "display description of the room and the pre-loaded contracts."
const roomInfo = (addChatEntry, metadata, contractList) => {
   return addChatEntry({
        type: "roomInfo",
        text: {metadata: metadata,
               contracts: util.generateContractDescriptions(contractList)
              }
    })
}

var nickUsage= " Usage: \"/nick set address name\" sets nickname\n \"/nick list\" lists nicknames"
docs["/nick"] = "nickname an address" + nickUsage
const nick = (addChatEntry, setNick, nicknames, args) => {
    let command = args[0]
    switch(command) {
        case "set":
            let address = args[1].toLowerCase()
            let nickname = args[2] || null

            if(!setNick(address, nickname)) {
                return addChatEntry({text:"/nick set: Invalid address, it should begin with 0x and be 40 characters long", type: "notification"})
            }
            return
        case "list":
            //pass object value not reference so chatEntry doesn't rerender on change
            return addChatEntry({type: "nicknames", text: JSON.parse(JSON.stringify(nicknames))})
        default:
            return addChatEntry({text: nickUsage, type: "notification"})
    }
}

var loadUsage = " Usage: \"/loadContract name address abi\""
docs["/loadContract"] = "load a contract into the chatroom." + loadUsage
const loadContract = (addChatEntry, addContractInfo, args) => {
    if (args.length !== 3) {
        return addChatEntry({text: "Wrong number of /sarguments. Correct Usage:" + "\n" + "\t" + loadUsage, type: "message"})
    }
    //parse string into Json
    try {
        var abi = JSON.parse(args[2])
    } catch(e){
        return addChatEntry({text: e.toString(), type:"notification"})
    }
    let added = addContractInfo({
        name: args[0],
        abi: abi,
        address: args[1]
    })
    if (added) {
        return addChatEntry({text:"Contract added. Type /info to see general information and all added contracts.", type:"notification"})
    }
}

var sendUsage = "\"/send amountInEther recipientAddress\""
docs["/sendEther"] = "send ether to an address. Usage: " + sendUsage
const sendEther = (addChatEntry, send, getUserAddress, eth, args) => {
    let from = getUserAddress()
    if (!from) {return}
    //check whether arguments make sense:
    if (!args[0]) {
        return addChatEntry({text: "You need to provide an amount. Correct Usage:" + "\n" + "\t" + sendUsage, type: "notification"})
    }
    if (!args[1]) {
        return addChatEntry({text: "You need to provide an address. Correct Usage:" + "\n" + "\t" + sendUsage, type: "notification"})
    }
    var address = args[1]
    const amount = args[0]
    try {
        var amountWei = unit.toWei(amount, "ether").toString()
    } catch(e) {
        if(e.toString().indexOf('invalid number value') !== -1) {
            return addChatEntry({type: "notification", text:"Wrong argument for /sendEther: \""+amount+"\" is not a valid amount"})
        }
    }
    eth.sendTransaction({
        from: from,
        to: address,
        value: amountWei,
        data: ''
    }).then(function(txreceipt) {
        var message = "/sendEther: You sent " + amount + " ether to " + address
        send({cmd:'sendEthTransaction', receipt: txreceipt, recipient: address})
        addChatEntry({text: message, type: "notification"})
        addChatEntry({text: txreceipt, type: "txHash"})
    }).catch(function(e) {
        var error = e.toString()
        if (error.indexOf('formatting inputs') !== -1 && error.indexOf('hex string') !== -1 ){
            console.log(e)
            addChatEntry({type: "notification", text:"Wrong argument for /sendEther: \"" + address + "\" is not a valid address"})
        }
        else {
            console.log(error)
            addChatEntry({type: "notification", text:"Error: Check the console for more information."})
        }
    })
}

// calls the function named args[0] of 'contract' with args[1:] as arguments
// example for TokenRoom: /token transfer 0xeC70a161C6283BCec515a0a0C0bf11328f908602 2
let exampleUsage = "\"/token transfer toAddress 100 \""
let sendTxUsage = " Usage: \"/contractName functionName arguments\""
docs["/<contractName>"] = "interacts with the given contract." + sendTxUsage
const sendTransaction = (addChatEntry, getUserAddress, eth, contract, args) => {
    var functionName = args[1]
    var args = args.slice(2)
    // get the function information
    var contractFunction = contract.abi.filter(function(obj){
        return obj.name === functionName && obj.type === 'function'
    })

    if(contractFunction.length !== 0){
        //only continue when metamask is unlocked
        var from = getUserAddress()
        if (!from) { return }

        // call function on contract with all arguments
        let contractInstance = eth.contract(contract.abi, "" ).at(contract.address)
        contractInstance[functionName](
            ...args.concat({from:from})
        ).then( (result) => {
            var text = functionName + "(" + args.join(', ') + ")"

            switch(true)
            {
                // the returned object is a number or address (e.g. because the command was a call)
                case result[0].constructor.name === 'BN' || util.isAddress(result[0]):
                    return addChatEntry({text: text +  " : "+ result[0].toString(), type: "notification"})

                // the returned object is a receipt
                case typeof result !== 'object':
                    return addChatEntry({text: result, type: "txHash"})

                default:
                    return addChatEntry({text: text+ result[0], type: "notification"})
            }
        }).catch( (e) => {
            addChatEntry({type: "notification", text: e.toString()})
        })
    } else {
        addChatEntry({text: "The function you're calling does not exist. See /info for the contracts ABI.", type: "notification"})
    }
}

let eventExample = "\"/events token Transfer _to:0xaec0b8a94ec86fdac2ccf53d5753fd238d3cd871\""
var eventUsage = " Usage: \"/events contractName [eventName [argName:argValue]]\"" // + "\n" + "Example: " + eventExample
//TODO properly formatted /help entries so that this can be displayed
docs["/events"] = "display past events for a specific contract." + eventUsage
const events = async (addChatEntry, w3, contracts, args) => {

    // check if contract, eventName & filterArguments are specified
    let result = await util.checkEventParams(contracts, args[0], args[1], args.slice(2))

    // ...correctly?
    if (!result[0]) {
        addChatEntry({text: result[1] , type:"notification"})
        return {}
    }
    let contractName = args[0]
    let contract = w3.eth.contract(contracts[contractName].abi).at(contracts[contractName].address)

    let blockParams =  {fromBlock: 0, toBlock: 'latest'}

    let eventName = result[1].eventName
    let event;
    if (eventName === "All") {
        event = contract.allEvents(blockParams)
    } else {
        let filterParams = result[1].filterParams
        event = contract[eventName](filterParams, blockParams)
    }
    event.get(function(error, logs) {
        if (!error) {
            console.log("logs",logs)
            addChatEntry({
                text: {
                    logs: logs,
                    from: contractName,
                    eventName: eventName || "All",
                    filter: args.slice(2)
                },
                type: "eventLogs"
            })
        }
    })
    event.stopWatching()
}


var whisperUsage = "\"/whspr recipientAddress yourText\""
docs["/whspr"] = "Send a private message that will not be saved." + whisperUsage
const whisper = async (addChatEntry, send, loggedIn, args) => {
    if (!loggedIn) {return}
    if (args[0] && args[1]) {
        send({cmd:"whisper", text:args.slice(1).join(' '), to:args[0]})
    } else {
        addChatEntry({text: "supply recipient and text", type:"notification"})
    }
}

export default {
    getOnline,
    help,
    sendEther,
    roomInfo,
    sendTransaction,
    events,
    loadContract,
    nick,
    whisper
}
