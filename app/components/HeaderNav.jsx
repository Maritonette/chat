import React from 'react';

const HeaderNav = (props) => (
	<div className="header-nav" style={{margin: '5px', height: '25px'}}>
	    <span id="title" style={{paddingTop:'5px', fontSize: '20px'}} >
          <b>&emsp; darq.chat </b>
      </span>
	    <span style={{textAlign: 'right',
                    float: 'right'
      }}>
          <LoginButton  login={props.login} logout={props.logout} loggedIn={props.loggedIn}/>
	    </span>
	</div>
)

function LoginButton (props) {
    let buttonFunction = props.login
    let buttonText = "login"

    if(props.loggedIn){
        buttonFunction = props.logout
        buttonText = "logout"
    }

    return <button id="login" style ={{ padding: '2px',
                                        backgroundColor: 'inherit',
                                        border: 'none',
                                        fontSize: '15px',
                                        color: 'blue',
                                        cursor: 'pointer'
    }}
                   onClick={buttonFunction}>{buttonText}</button>
}

export default HeaderNav;
