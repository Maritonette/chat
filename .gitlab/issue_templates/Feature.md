**NOTE: This form** is for entering **non-bug** issues

<!-- If you are reporting a BUG please use the bug template instead --> 
<!-- See our CONTRIBUTING guide to learn how we work with contributions, and thus to make the most of your awesome time and effort -->

* Please provide a concise description of the issue in the Title above

--------------------------------------------------------------------------------
## Description of Issue:  
<!-- Provide a description of the change or addition you are proposing -->
  * **Current Behavior** (describe _what currently happens_, at present):

  * **Expected Behavior** (describe _what you expect to happen, instead_, once the issue is resolved):


## Motivation and Context:
<!-- How has this issue affected you? What are you trying to accomplish? --> 
<!-- Providing context, or a specific use case, -->
<!-- helps us come up with a solution that is most useful in the real world -->


## Related Issues:
<!-- Reference open issues by their issue number --> 
<!-- If a particular issue has not yet been documented, please create it separately -->
  * **Depends On** (list any issues that should be completed BEFORE addressing this one):

  * **Enables** (list any issues that could or should be addressed AFTER this one):  
    <!-- this could include new features that would be enabled by completing this issue -->


## Links:
<!-- Post links below to any other info, resources, or examples --> 
<!-- that may be of help in understanding or resolving the issue -->


## Notify Relevant Individuals:
<!-- Add the gitlab handles of specific individuals here to notify them directly -->


--------------------------------------------------------------------------------
/label ~feature

<!-- Add further specification to the issue using the drop-down lists below, as appropriate -->

