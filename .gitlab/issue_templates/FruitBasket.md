### FruitBasket: a Meta-Issue-Template for Discussions
**NOTE: This Issue Template is for framing project discussions that fall outside existing templates.** 
It's sort of a meta-issue-template, since it will include issues about issues. 
Project discussion topics, like fruits, can be of different shapes and sizes, 
as well as degrees of ripeness. Hopefully this template will help us catch them all 
-- for consideration, discussion, and collective decision-making. 
This template and format will likely evolve with use. 
And it remains an open question whether this will be the best medium for this activity 
(for example, there may be some issues which don't lend themselves 
to a tidy resolution and may remain open, indefinitely). 
But it is an attempt to bring into our Gitlab repo the sorts of project discussions 
that are often occurring outside of it, scattershot, via chat threads and collaborative text documents. 

<!-- If you are submitting another issue** like a bug, new feature or some other proposed change, -->
<!-- please use one of the other Issue Templates -->

* Please provide a concise description of the topic for discussion in the Title above

--------------------------------------------------------------------------------

## Description
<!-- Provide a more detailed description of the issue/topic you are proposing -->
<!-- Describe the "Current State" of things, or why this topic is important to address -->
<!-- Describe the "Expected State" of things, or the result you expect to come from addressing the issue -->


## Motivation and Context
<!-- Provide any context or specifics here (examples, use cases, user stories) -->
<!-- to flesh out the significance of the issue -->


## Possible Implementation
<!-- Suggest a concrete way to explore or implement the issue -->
<!-- Include any details helpful to understanding what the implementation would look like, or entail -->


## Other Info
<!-- Include any other relevant data, links, resources, screenshots, etc. -->


## Assign Collaborators
<!-- Since gitlab only allows for one official Assignee, you can specify collaborators here for this discussion -->
<!-- (Add them below, by their gitlab handle) -->

--------------------------------------------------------------------------------

/label ~discussion

**Disclaimer:** This form is a work in progress. We aim to shape it into a "form" that will be of maximum utility.  
Feedback on its components is welcomed!

<!-- Specify your issue further using the drop-down lists below, as appropriate -->

