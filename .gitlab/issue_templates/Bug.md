**NOTE: This form** is for reporting `bugs` **ONLY**

<!-- If you are NOT reporting a bug please use a different issue template -->
<!-- See our CONTRIBUTING guide to learn how we work with contributions, and thus to make the most of your awesome time and effort -->

* Please provide a concise description of the issue in the Title above

--------------------------------------------------------------------------------
## Current Bug Behavior
<!-- What is the behavior that currently happens, that is not expected? -->


## Expected Correct Behavior 
<!-- What is the behavior you should see instead? -->


## Possible Source of Bug
<!-- If you can, provide a link to the line of code that might be responsible for the problem -->


## Steps to Reproduce the Problem
<!-- How one can reproduce the issue - this is very important -->
<!--Provide a link to a live example, or an unambiguous set of steps to reproduce this bug -->
1.  
2.  
3.  


## Possible Solution
<!-- Suggest a bug fix, if possible --> 
<!-- Describe the change you are proposing, and include any key implementation details -->


## Relevant Logs and/or Screenshots
<!-- Paste any relevant logs - please use code blocks (```) to format console output, --> 
<!-- logs, and code, as it's very hard to read otherwise. -->


## Links
<!-- Post any links below to other info, resources, or examples that may be of help --> 
<!-- in understanding the bug or designing a fix -->


## Notify Relevant Individuals
<!-- Add the gitlab handles of specific individuals here to notify them directly -->


--------------------------------------------------------------------------------
/label ~bug

<!-- Add further specification to the issue using the drop-down lists below, as appropriate -->

