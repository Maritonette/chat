var express = require('express')
const path = require('path');
var db = require('./db');

var port =  process.env.PORT || 8000
console.log(port)

db.setup();

var app = express()
app.use(function (req, res, next) {
    return next();
});

app.use('/', express.static(path.join(__dirname, '../static')))
require('./server.js').server(app)

app.listen(port)
